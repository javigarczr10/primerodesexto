/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author safoe
 */
public class EmpleadoBase extends Empleado implements Impuesto {
       private float pagoDiario;
       private float diasTrabajados;

    public EmpleadoBase() {
        this.pagoDiario=0.0f;
        this.diasTrabajados=0.0f;
    }

    public EmpleadoBase(float pagoDiario, float diasTrabajados, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
       

       
    @Override
    public float calcularpago() {
        return this.diasTrabajados*this.pagoDiario;
    }

    @Override
    public float calcularImpuesto() {
        float impuestos=0;
        if(this.calcularpago()>5000) impuestos= this.calcularpago()*0.16f;
        return impuestos;
    }
    
}
