/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author safoe
 */
public class EmpleadoEventual extends Empleado{
private float PagoHora;
private float HorasTrabajadas;
    

    public EmpleadoEventual() {
        this.PagoHora=0.0f;
        this.HorasTrabajadas=0.0f;
    }

    public EmpleadoEventual(float PagoHora, float HorasTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.PagoHora = PagoHora;
        this.HorasTrabajadas = HorasTrabajadas;
    }

    public float getPagoHora() {
        return PagoHora;
    }

    public void setPagoHora(float PagoHora) {
        this.PagoHora = PagoHora;
    }

    public float getHorasTrabajadas() {
        return HorasTrabajadas;
    }

    public void setHorasTrabajadas(float HorasTrabajadas) {
        this.HorasTrabajadas = HorasTrabajadas;
    }
    
        
    
            
 

    @Override
    public float calcularpago() {
                return this.HorasTrabajadas * this.PagoHora;
       
    }
    
}

