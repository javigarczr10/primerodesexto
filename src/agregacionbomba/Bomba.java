/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author safoe
 */
public class Bomba {
    private int NumBomba ;
    private float Capacidad;
    private float Contador;
    private Gasolina gasolina;

    public Bomba() {
        this.NumBomba=0;
        this.Capacidad=0.0f;
        this.Contador=0.0f;
        this.gasolina = new Gasolina();
        
    }

    public Bomba(int NumBomba, float Capacidad, float Contador, Gasolina gasolina) {
        this.NumBomba = NumBomba;
        this.Capacidad = Capacidad;
        this.Contador = Contador;
        this.gasolina = gasolina;
    }
    
    public Bomba(Bomba bomba){
        this.NumBomba=bomba.NumBomba;
        this.Capacidad=bomba.Capacidad;
        this.Contador=bomba.Contador;
        this.gasolina=bomba.gasolina;
        
    }

    public int getNumBomba() {
        return NumBomba;
    }

    public void setNumBomba(int NumBomba) {
        this.NumBomba = NumBomba;
    }

    public float getCapacidad() {
        return Capacidad;
    }

    public void setCapacidad(float Capacidad) {
        this.Capacidad = Capacidad;
    }

    public float getContador() {
        return Contador;
    }

    public void setContador(float Contador) {
        this.Contador = Contador;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }

    
    
     public float calcularInventario(){
        return this.Capacidad - this.Contador;
        
    }
    public boolean realizarVenta(float cantidad){
        boolean exito = false;
        if (cantidad <= this.calcularInventario())
            exito = true;
        
        return exito;
        
    }
     public float calcularImporte(float precio){
        return this.Contador * precio;
    }
}
